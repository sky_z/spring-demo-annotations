package com.ko.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationBeanScopeDemoApp {

	public static void main(String[] args) {

		// load spring config file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		// retrieve bean from spring container
		Hero theHero = context.getBean("batman", Hero.class);

		Hero alphaHero = context.getBean("batman", Hero.class);

		// check if they are the same object
		boolean result = (theHero == alphaHero);

		// print out the result
		System.out.println("\nPointing to the same object: " + result);

		System.out.println("\nMemory location for the theHero: " + theHero);

		System.out.println("\nMemory location for the alphaHero: " + alphaHero + "\n");

		// close the context
		context.close();

	}

}
