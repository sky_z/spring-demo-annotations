package com.ko.springdemo;

import org.springframework.beans.factory.annotation.Value;

public class Aquaman implements Hero {

	private HelpService helpService;

	@Value("${foo.name}")
	private String name;

	@Value("${foo.team}")
	private String team;

	public Aquaman(HelpService theHelpService) {
		helpService = theHelpService;
	}

	@Override
	public String getSuperpower() {
		return "Not as good as Poseidon.";
	}

	@Override
	public String getHelp() {
		return helpService.getHelp();
	}

	// getters
	public String getName() {
		return name;
	}

	public String getTeam() {
		return team;
	}

}
