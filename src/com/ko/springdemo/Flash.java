/*
 * package com.ko.springdemo;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Component;
 * 
 * @Component public class Flash implements Hero {
 * 
 * private HelpService helpService;
 * 
 * // @Autowired public Flash(HelpService theHelpService) {
 * System.out.println(">> Flash: inside default constuctor"); helpService =
 * theHelpService; }
 * 
 * @Override public String getSuperpower() { return "Eating cornflakes"; }
 * 
 * @Override public String getHelp() { return helpService.getHelp(); }
 * 
 * }
 */