package com.ko.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpidermanJavaConfigDemoApp {

	public static void main(String[] args) {

		// read spring config file
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpidermanConfig.class);

		// get the bean from spring container
		Hero theHero = context.getBean("spiderman", Hero.class);

		// call a method on the bean
		System.out.println(theHero.getSuperpower());

		// call method to get the help
		System.out.println(theHero.getHelp());

		// close the context
		context.close();
	}

}
