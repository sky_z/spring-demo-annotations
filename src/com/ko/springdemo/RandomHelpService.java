package com.ko.springdemo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class RandomHelpService implements HelpService {

	// create an array of string

	// private String[] data = { "Buying Vodka bottle", "Bring home after Night
	// out", "Bring food from outside" };

	ArrayList<String> result = new ArrayList<>();

	@PostConstruct
	public void loadData() {
		try (FileReader f = new FileReader("/home/slay/Java/spring-demo-annotations/src/help.txt")) {
			StringBuffer sb = new StringBuffer();
			while (f.ready()) {
				char c = (char) f.read();
				if (c == '\n') {
					result.add(sb.toString());
					sb = new StringBuffer();
				} else {
					sb.append(c);
				}
			}
			if (sb.length() > 0) {
				result.add(sb.toString());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// create random number generator
	private Random myRandom = new Random();

	@Override
	public String getHelp() {
		// call method for filling arraylist
		// loadData();
		// pick a random string from the array
		int index = myRandom.nextInt(result.size());

		String theHelp = result.get(index);

		return theHelp;
	}

}
