package com.ko.springdemo;

public class Spiderman implements Hero {

	private HelpService helpService;

	public Spiderman(HelpService theHelpService) {
		helpService = theHelpService;
	}

	@Override
	public String getSuperpower() {
		return "Having crush on Mary Jane.";
	}

	@Override
	public String getHelp() {
		return helpService.getHelp();
	}

}
