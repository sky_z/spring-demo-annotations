package com.ko.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AquamanJavaConfigDemoApp {

	public static void main(String[] args) {

		// read spring config file
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HeroConfig.class);

		// get the bean from spring container
		Aquaman theHero = context.getBean("aquaman", Aquaman.class);

		// call a method on the bean
		System.out.println(theHero.getSuperpower());

		// call method to get the help
		System.out.println(theHero.getHelp());

		// call new methods which has values from props injected values
		System.out.println("name: " + theHero.getName());
		System.out.println("team: " + theHero.getTeam());

		// close the context
		context.close();
	}

}
