package com.ko.springdemo;

public interface Hero {

	public String getSuperpower();
	
	public String getHelp();

}
