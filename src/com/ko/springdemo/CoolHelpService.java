package com.ko.springdemo;

import org.springframework.stereotype.Component;

@Component
public class CoolHelpService implements HelpService {

	@Override
	public String getHelp() {
		return "You are lucky that i can help you!";
	}

}
