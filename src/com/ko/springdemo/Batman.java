package com.ko.springdemo;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Batman implements Hero {

	@Autowired
	@Qualifier("randomHelpService")
	private HelpService helpService;

	// default constructor
	public Batman() {
		System.out.println(">> Batman: inside default constructor");
	}

	// define init method
	@PostConstruct
	public void doStartupStuff() {
		System.out.println(">> Batman: doStartupStuff()");
	}

	// define destroy method
	@PreDestroy
	public void doCleanupStuff() {
		System.out.println(">> Batman: doCleanupStuff()");
	}

	@Override
	public String getSuperpower() {
		return "Having a butler ";
	}

	@Override
	public String getHelp() {
		return helpService.getHelp();
	}

}
