/*
 * package com.ko.springdemo;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Component;
 * 
 * @Component public class Superman implements Hero {
 * 
 * private HelpService helpService;
 * 
 * // default constructor public Superman() {
 * System.out.println(">> Superman: inside default constructor"); }
 * 
 * // @Autowired public void doSomeSillyThings(HelpService theHelpService) {
 * System.out.println(">> Superman: inside default doSomeSillyThings() method");
 * helpService = theHelpService; }
 * 
 * @Override public String getSuperpower() { return "Underpants over pants "; }
 * 
 * @Override public String getHelp() { return helpService.getHelp(); }
 * 
 * }
 */