package com.ko.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
// @ComponentScan("com.ko.springdemo")
@PropertySource("classpath:hero.properties")
public class SpidermanConfig {

	// define bean for bad help service
	@Bean
	public HelpService jobHelpService() {
		return new JobHelpService();
	}

	// define bean for Spiderman AND inject dependency
	@Bean
	public Hero spiderman() {
		return new Spiderman(jobHelpService());
	}

}
