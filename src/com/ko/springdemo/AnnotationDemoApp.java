package com.ko.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AnnotationDemoApp {

	public static void main(String[] args) {

		// read spring config java class
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HeroConfig.class);

		// get the bean from spring container
		Hero theHero = context.getBean("batman", Hero.class);

		// call a method on the bean
		System.out.println(theHero.getSuperpower());

		// call method to get the help
		System.out.println(theHero.getHelp());

		// close the context
		context.close();
	}

}
